1) Service  for login/send otp

    **API** :  http://vms-corporation.com/ggh/web_service/login
    
    **Method** :post 
    
    **Params** : mobile_no : 7897897898
    
    **Response**
    
    {
        "code": 200,
        "status": "success",
        "data": {
            "id": "44",
            "mobile_no": "8899889565",
            "otp": "7347",
            "created_date": "2019-08-03 13:05:49"
        }
    }

2) Service for List of complains

    **API** : http://vms-corporation.com/ggh/web_service/complain_list
    
    **Method** :post 
    
    **Params** : user_id : 2
    
    **Response**
    
    {
        "code": 200,
        "status": "success",
        "data": {
            "complain_list": [
                {
                    "id": "10009",
                    "user_id": "3",
                    "category_id": "2",
                    "message": "desc ",
                    "image": "http://vms-corporation.com/ggh/public/webservice/6113541888.png",
                    "status": "p",
                    "created_date": "2019-08-03 13:08:32",
                    "category_name": "Social",
                    "complain_id": "CN10009"
                },
                {
                    "id": "10034",
                    "user_id": "3",
                    "category_id": "2",
                    "message": "desc desdsdsads",
                    "image": "http://vms-corporation.com/ggh/public/webservice/6074115986.png",
                    "status": "p",
                    "created_date": "2019-08-03 13:38:26",
                    "category_name": "Social",
                    "complain_id": "CN10034"
                },
                {
                    "id": "10035",
                    "user_id": "3",
                    "category_id": "2",
                    "message": "desc desdsdsads",
                    "image": "http://vms-corporation.com/ggh/public/webservice/8146559185.png",
                    "status": "p",
                    "created_date": "2019-08-03 13:40:55",
                    "category_name": "Social",
                    "complain_id": "CN10035"
                }
            ],
            "category_list": [
                {
                    "id": "1",
                    "category_name": "Hospital"
                },
                {
                    "id": "2",
                    "category_name": "Social"
                }
            ]
        }
    }
    
3) Service for create/add complains

    **API** : http://vms-corporation.com/ggh/web_service/create
    
    **Method** :post 
    
    **Params** :
    
    category_id:2
    
    user_id:2
    
    message: Message desc.
    
    image: encoded image data
    
    **Response**
    
    Success Reponse :: 
    
        {
            "code": 200,
            "status": "success",
            "data": "Complain registed successfully"
        }
    Eror Reponse :: 
    
        {
            "code": 100,
            "status": "fails",
            "data": "Please insert all details"
        }

4) Service  for delete complain

    **API** :  http://vms-corporation.com/ggh/web_service/delete?id=10034
    
    **Method** : get
    
    **Response**
    
    {
        "code": 200,
        "status": "success",
        "data": "Complain deleted successfully"
    }
    
5) Service  for edit complain

    **API** :  http://vms-corporation.com/ggh/web_service/edit?id=10034
    
    **Method** : get
    
    **Response**
    
    {
        "code": 200,
        "status": "success",
        "data": {
            "id": "10009",
            "user_id": "2",
            "category_id": "2",
            "message": "desc desdsdsads",
            "image": "http://vms-corporation.com/ggh/public/webservice/4117981562.png",
            "status": "p",
            "created_date": "2019-08-03 13:08:32"
        }
    }
    
6) Service for update complains

    **API** : http://vms-corporation.com/ggh/web_service/update/10034
    
    **Method** :post 
    
    **Params** :
    
    category_id:2
    
    user_id:2
    
    message: Message desc.
    
    image: encoded image data (for update its  optional)

    
    **Response**
    
    {
        "code": 200,
        "status": "success",
        "data": "Complain updated successfully"
    }

7) Service for get category

    **API** : http://vms-corporation.com/ggh/web_service/category
    
    **Method** :get 
    
    
    **Response**
    
    {
    "code": 200,
    "status": "success",
    "data": [
        {
            "id": "1",
            "category_name": "Hospital"
        },
        {
            "id": "2",
            "category_name": "Social"
        }
    ]
}

**Bulk SMS API**

1) Bulk SMS API

    **API** : http://vms-corporation.com/ggh/web_service/bulk_sms
    
    **Method** :post 
    **Params** :
    
    mobile_nos:8877889954
    
    content: hello
    
    **Response**
    
    {"code":200,"status":"success","data":"SMS Sent successfully"}
    
    

Note :: Complain Status.

p   - Pending

pro - In Progress

c   - Cancelled

s   - Solved

