-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 02, 2019 at 06:29 PM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `complain`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `token` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `email`, `password`, `token`) VALUES
(1, 'chandspatel888@gmail.com', '123456', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `id` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `category_name`) VALUES
(1, 'Hospital'),
(2, 'Social');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_complain`
--

CREATE TABLE `tbl_complain` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `image` varchar(255) NOT NULL,
  `status` enum('p','pro','c','s') NOT NULL DEFAULT 'p',
  `created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_complain`
--

INSERT INTO `tbl_complain` (`id`, `user_id`, `category_id`, `message`, `image`, `status`, `created_date`) VALUES
(10002, 1, 2, 'desc', 'image', 'p', '2019-07-31 23:44:17'),
(10003, 1, 1, 'desc ', 'image', 's', '2019-07-31 23:44:17'),
(10004, 1, 2, 'desc', 'image', 'pro', '2019-07-31 23:44:17'),
(10005, 1, 1, 'desc ', 'image', 'c', '2019-07-31 23:44:17'),
(10008, 2, 2, 'desc desdsdsads', 'image', 'p', '2019-08-01 21:54:18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_images`
--

CREATE TABLE `tbl_images` (
  `id` int(11) NOT NULL,
  `complain_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `mobile_no` int(11) NOT NULL,
  `otp` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `mobile_no`, `otp`, `created_date`) VALUES
(3, 878787878, '6637', '2019-07-31 23:36:10'),
(6, 87878787, '1144', '2019-07-31 23:32:17'),
(7, 87878787, '1144', '2019-07-31 23:32:17'),
(8, 87878787, '1144', '2019-07-31 23:32:17'),
(9, 87878787, '1144', '2019-07-31 23:32:17'),
(10, 87878787, '1144', '2019-07-31 23:32:17'),
(11, 87878787, '1144', '2019-07-31 23:32:17'),
(12, 87878787, '1144', '2019-07-31 23:32:17'),
(13, 87878787, '1144', '2019-07-31 23:32:17'),
(14, 87878787, '1144', '2019-07-31 23:32:17'),
(15, 87878787, '1144', '2019-07-31 23:32:17'),
(16, 87878787, '1144', '2019-07-31 23:32:17'),
(17, 87878787, '1144', '2019-07-31 23:32:17'),
(18, 87878787, '1144', '2019-07-31 23:32:17'),
(19, 87878787, '1144', '2019-07-31 23:32:17'),
(20, 87878787, '1144', '2019-07-31 23:32:17'),
(21, 87878787, '1144', '2019-07-31 23:32:17'),
(22, 87878787, '1144', '2019-07-31 23:32:17'),
(23, 87878787, '1144', '2019-07-31 23:32:17'),
(24, 87878787, '1144', '2019-07-31 23:32:17'),
(25, 87878787, '1144', '2019-07-31 23:32:17'),
(26, 87878787, '1144', '2019-07-31 23:32:17'),
(27, 87878787, '1144', '2019-07-31 23:32:17'),
(28, 87878787, '1144', '2019-07-31 23:32:17'),
(29, 87878787, '1144', '2019-07-31 23:32:17'),
(30, 87878787, '1144', '2019-07-31 23:32:17'),
(31, 87878787, '1144', '2019-07-31 23:32:17'),
(32, 87878787, '1144', '2019-07-31 23:32:17'),
(33, 87878787, '1144', '2019-07-31 23:32:17'),
(34, 87878787, '1144', '2019-07-31 23:32:17'),
(35, 87878787, '1144', '2019-07-31 23:32:17'),
(36, 87878787, '1144', '2019-07-31 23:32:17'),
(37, 87878787, '1144', '2019-07-31 23:32:17'),
(38, 87878787, '1144', '2019-07-31 23:32:17'),
(39, 87878787, '1144', '2019-07-31 23:32:17'),
(40, 87878787, '1144', '2019-07-31 23:32:17'),
(41, 87878787, '1144', '2019-07-31 23:32:17'),
(42, 87878787, '1144', '2019-07-31 23:32:17'),
(43, 87878787, '1144', '2019-07-31 23:32:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_complain`
--
ALTER TABLE `tbl_complain`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_images`
--
ALTER TABLE `tbl_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_complain`
--
ALTER TABLE `tbl_complain`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10009;

--
-- AUTO_INCREMENT for table `tbl_images`
--
ALTER TABLE `tbl_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
