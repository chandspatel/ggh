<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


$route['default_controller']    =   "home";
$route['admin']  =   "admin/login/index";
$route['admin/home']  =   "admin/index";
$route['admin/buslist']  =   "admin/article";
$route['copy.php'] = "Home/copy";
$route['404_override'] = '';
$route['logout.php'] = 'Home/logout';
$route['admin/logout.php'] = 'admin/Home/logout';

$route['web_service/login']  =   "web_service/login/index";
$route['web_service/complain_list']  =   "web_service/complainList/index";
$route['web_service/create']  =   "web_service/create/index";
$route['web_service/delete']  =   "web_service/create/delete";
$route['web_service/edit']  =   "web_service/create/edit";
$route['web_service/update/(:num)']  = "web_service/create/update/$1";
$route['web_service/category']  =   "web_service/complainList/category";
$route['web_service/bulk_sms']  =   "web_service/bulkSms/index";



/* End of file routes.php */
/* Location: ./application/config/routes.php */