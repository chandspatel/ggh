<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Familylogout extends CI_Controller 
{

  public function __construct()
        {
            parent::__construct();     
        }

	public function index()
	{   
	    $this->session->unset_userdata('familyuser');
		redirect(base_url().'admin/familylogin');
	}
	
}
