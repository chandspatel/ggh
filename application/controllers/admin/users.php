<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(empty($this->session->userdata('user'))){
			redirect(base_url().'admin/login');
		}

		$this->load->model('admin/users_model');
		$this->load->helper(array('form', 'url'));
	}
	public function index()
	{
			$data['users'] = $this->users_model->get_users();
			$data1['loginuser']	   = $this->session->userdata('user');
			$this->load->view('admin/includes/header',$data1);
			$this->load->view('admin/includes/page_header_sidebar');	  		
			$this->load->view('admin/users/index', $data);
			$this->load->view('admin/includes/footer');   
		
	}
	public function edit($surname_id){
		if(empty($surname_id))
		{
			redirect($_SERVER['HTTP_REFERER']);
		}
		$data['surname_id'] = $surname_id;
		$this->form_validation->set_rules('surname', ' Surname', 'required|trim|xss_clean|myAlpha');
	   
		if($this->form_validation->run() === FALSE)
		{
				$data1['loginuser']	   = $this->session->userdata('user');
				$data['surname'] = $this->users_model->get_single_surname_detail($surname_id);
				$this->load->view('admin/includes/header',$data1);
				$this->load->view('admin/includes/page_header_sidebar');	  		
				$this->load->view('admin/surname/edit', $data);
				$this->load->view('admin/includes/footer');
		}
		else
		{
		
			$data  = array();
			$where = array('surname_id' => $surname_id);
			$data  = array('surname' => $this->input->post('surname'), 
						   'created_date' => date('Y-m-d'));	
			$res = $this->users_model->update($data, $where);
			if($res)
			{
				$this->session->set_flashdata('success', '<div class="alert alert-success">Surname Updated Successfully.</div>');
			}
			else
			{
				$this->session->set_flashdata('success', '<div class="alert alert-success">Surname Not Updated Successfully.</div>');
			}
			redirect(base_url().'admin/surname/index');
		}
		
	}


		public function add()
		{
			$this->form_validation->set_rules('surname', 'Enter Surname', 'required|trim|xss_clean|myAlpha');
			
		   if($this->form_validation->run() === FALSE)
			{
				$data['surname']   = $this->users_model->get_users();
				$data1['loginuser']	   = $this->session->userdata('user');
				$this->load->view('admin/includes/header',$data1);
				$this->load->view('admin/includes/page_header_sidebar');	  		
				$this->load->view('admin/surname/add',$data);
				$this->load->view('admin/includes/footer');
			}
			else
			{	
				$data    = array('surname' => $this->input->post('surname'),			
								 'created_date'=>date('Y-m-d'));
				$res = $this->users_model->add($data);
				if($res)
				{
					$this->session->set_flashdata('success', '<div class="alert alert-success">Surname Add Successfully.</div>');
				}
				else
				{
					$this->session->set_flashdata('success', '<div class="alert alert-danger">Surname Not Add Successfully.</div>');
				}
				redirect(base_url().'admin/surname/index');
			}       
		}
		
		 public function delete($id)
		{
			$res =   $this->users_model->delete($id);
			if($res)
			{
				$this->session->set_flashdata('success', '<div class="alert alert-success">User Deleted Successfully.</div>');
			}
			else
			{
			  $this->session->set_flashdata('success', '<div class="alert alert-danger">User  Not Deleted.</div>');
			}
			redirect(base_url().'admin/users/index');
		}
	  
		public function logout()
		{
			$this->session->unset_userdata('user');
			$this->session->sess_destroy();
			redirect(base_url());
		}   
			
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */