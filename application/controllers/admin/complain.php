<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Complain extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(empty($this->session->userdata('user'))){
			redirect(base_url().'admin/login');
		}
		$this->load->library("pagination");

		$this->load->model('admin/complain_model');
		$this->load->helper(array('form', 'url'));
	}
	public function index()
	{
			$data1['loginuser']	   = $this->session->userdata('user');
			$wherelist = array();
			if (isset($_GET['type']) && !empty($_GET['type'])) {
				$wherelist['status'] = $_GET['type'];
			}
			$startPage =0;

			$complainData = $this->complain_model->get_complains($wherelist,$startPage);
			$complainDataArr = [];
			$statusArr = [
				'p' => 'Pending',
				'pro' => 'In Progress',
				'c' => 'Cancelled',
				's' => 'Solved',
			];
			foreach ($complainData as $key => &$value) {
				$optionHtml = '';
			
				foreach ($statusArr as $skey => $sValue) {
					$selected = "";
					if ($skey == $value['status']) {
						$selected = "selected";

					}
					$optionHtml .="<option $selected value='".$skey."'>".$sValue."</option>";
				}
				$value['status']= $optionHtml;
				$value['complain_no']= 'CMP'.$value['id'];

			}
			 // start of pagination
			$config['base_url'] = site_url.'admin/complain/';
			$config['per_page'] = 3;
			$config['num_links'] = 7;
			$config['total_rows'] = 10;//count($familydetail_search_data);

			$config['total_rows'];
			$this->pagination->initialize($config);

			$data['complain'] = $complainData;
			$this->load->view('admin/includes/header',$data1);
			$this->load->view('admin/includes/page_header_sidebar');	  		
			$this->load->view('admin/complain/index', $data);
			$this->load->view('admin/includes/footer');   
		
	}
	public function edit($surname_id){
		if(empty($surname_id))
		{
			redirect($_SERVER['HTTP_REFERER']);
		}
		$data['surname_id'] = $surname_id;
		$this->form_validation->set_rules('surname', ' Surname', 'required|trim|xss_clean|myAlpha');
	   
		if($this->form_validation->run() === FALSE)
		{
				$data1['loginuser']	   = $this->session->userdata('user');
				$data['surname'] = $this->users_model->get_single_surname_detail($surname_id);
				$this->load->view('admin/includes/header',$data1);
				$this->load->view('admin/includes/page_header_sidebar');	  		
				$this->load->view('admin/surname/edit', $data);
				$this->load->view('admin/includes/footer');
		}
		else
		{
		
			$data  = array();
			$where = array('surname_id' => $surname_id);
			$data  = array('surname' => $this->input->post('surname'), 
						   'created_date' => date('Y-m-d'));	
			$res = $this->users_model->update($data, $where);
			if($res)
			{
				$this->session->set_flashdata('success', '<div class="alert alert-success">Surname Updated Successfully.</div>');
			}
			else
			{
				$this->session->set_flashdata('success', '<div class="alert alert-success">Surname Not Updated Successfully.</div>');
			}
			redirect(base_url().'admin/surname/index');
		}
		
	}


		public function add()
		{
			$this->form_validation->set_rules('surname', 'Enter Surname', 'required|trim|xss_clean|myAlpha');
			
		   if($this->form_validation->run() === FALSE)
			{
				$data['surname']   = $this->users_model->get_users();
				$data1['loginuser']	   = $this->session->userdata('user');
				$this->load->view('admin/includes/header',$data1);
				$this->load->view('admin/includes/page_header_sidebar');	  		
				$this->load->view('admin/surname/add',$data);
				$this->load->view('admin/includes/footer');
			}
			else
			{	
				$data    = array('surname' => $this->input->post('surname'),			
								 'created_date'=>date('Y-m-d'));
				$res = $this->users_model->add($data);
				if($res)
				{
					$this->session->set_flashdata('success', '<div class="alert alert-success">Surname Add Successfully.</div>');
				}
				else
				{
					$this->session->set_flashdata('success', '<div class="alert alert-danger">Surname Not Add Successfully.</div>');
				}
				redirect(base_url().'admin/surname/index');
			}       
		}
		
		 public function delete($id)
		{
			$res =   $this->complain_model->delete($id);
			if($res)
			{
				$this->session->set_flashdata('success', '<div class="alert alert-success">Complain Deleted Successfully.</div>');
			}
			else
			{
			  $this->session->set_flashdata('success', '<div class="alert alert-danger">Complain  Not Deleted.</div>');
			}
			redirect(base_url().'admin/complain/index');
		}
	  
		public function logout()
		{
			$this->session->unset_userdata('user');
			$this->session->sess_destroy();
			redirect(base_url());
		}
		public function updateStatus(){
			if(empty($_POST))
			{
				echo json_encode(["code"=>100]);exit;
			}
			$where = array('id' => $this->input->post('id'));
			$data  = array('status' => $this->input->post('val'));

			$res = $this->db->update('tbl_complain', $data, $where);
			if($res)
			{
				echo json_encode(["code"=>100, 'msg' =>  'Status Updated Successfully']);exit;
			}
			else
			{
				echo json_encode(["code"=>100]);exit;
			}
		}

		public function getDetails(){
			$id = $this->input->post("id");
			if (!empty($id)) {
				$where=array("id"=>$id);

				$this->db->select('tbl_complain.*,tbl_users.mobile_no, tbl_category.category_name_eng,tbl_category.category_name_guj');
				$this->db->from('tbl_complain');
				$this->db->join('tbl_users','tbl_users.id = tbl_complain.user_id');
				$this->db->join('tbl_category','tbl_complain.category_id = tbl_category.id AND tbl_complain.id ='.$id.'');
				$return = $this->db->get()->row();


				if (!empty($return->image) && file_exists(APP_WEBSERVICE_IMAGE_DIR.$return->image)) {
					$imageFile = APP_WEBSERVICE_IMAGE.$return->image;
				} else {
					$imageFile = APP_WEBSERVICE_IMAGE.'noimg.png';
				}
				$statusArr = [
					'p' => 'Pending',
					'pro' => 'In Progress',
					'c' => 'Cancelled',
					's' => 'Solved',
				];
				$return->status = $statusArr[$return->status];
				$return->image = $imageFile;
				//print_r($return);exit;
				echo $this->load->view('admin/complain/modal',$return, true);
				
			}
		}
		
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */