<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class category extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(empty($this->session->userdata('user'))){
			redirect(base_url().'admin/login');
		}

		$this->load->model('admin/category_model');
		$this->load->helper(array('form', 'url'));
	}
	public function index()
	{
			$data['category'] = $this->category_model->get_category();
			$data1['loginuser']	   = $this->session->userdata('user');
			$this->load->view('admin/includes/header',$data1);
			$this->load->view('admin/includes/page_header_sidebar');	  		
			$this->load->view('admin/category/index', $data);
			$this->load->view('admin/includes/footer');   
		
	}
	public function edit($id){
		if(empty($id))
		{
			redirect($_SERVER['HTTP_REFERER']);
		}
		$data['id'] = $id;
		if(empty($_POST))
		{
				$data1['loginuser']	   = $this->session->userdata('user');
				$data['category'] = $this->category_model->get_single_category_detail($id);
				$this->load->view('admin/includes/header',$data1);
				$this->load->view('admin/includes/page_header_sidebar');
				$this->load->view('admin/category/edit', $data);
				$this->load->view('admin/includes/footer');
		}
		else
		{
		
			$data  = array();
			$where = array('id' => $id);
			$data  = array('category_name_eng' => $this->input->post('category_name_eng'),'category_name_guj' => $this->input->post('category_name_guj'));
			$res = $this->category_model->update($data, $where);
			if($res)
			{
				$this->session->set_flashdata('success', '<div class="alert alert-success">Category Updated Successfully.</div>');
			}
			else
			{
				$this->session->set_flashdata('success', '<div class="alert alert-success">Category Updated Successfully.</div>');
			}
			redirect(base_url().'admin/category/index');
		}
		
	}


		public function add()
		{
			
			if(empty($_POST))
			{
				$data['category']   = $this->category_model->get_category();
				$data1['loginuser']	   = $this->session->userdata('user');
				$this->load->view('admin/includes/header',$data1);
				$this->load->view('admin/includes/page_header_sidebar');	  		
				$this->load->view('admin/category/add',$data);
				$this->load->view('admin/includes/footer');
			}
			else
			{	
				$data    = array('category_name_eng' => $this->input->post('category_name_eng'),'category_name_guj' => $this->input->post('category_name_guj'));
				$res = $this->category_model->add($data);
				if($res)
				{
					$this->session->set_flashdata('success', '<div class="alert alert-success">Category Added Successfully.</div>');
				}
				else
				{
					$this->session->set_flashdata('success', '<div class="alert alert-danger">Category Not Added Successfully.</div>');
				}
				redirect(base_url().'admin/category/index');
			}
		}
		
		 public function delete($id)
		{
			$res =   $this->category_model->delete($id);
			if($res)
			{
				$this->session->set_flashdata('success', '<div class="alert alert-success">Category Deleted Successfully.</div>');
			}
			else
			{
			  $this->session->set_flashdata('success', '<div class="alert alert-danger">User  Not Deleted.</div>');
			}
			redirect(base_url().'admin/category/index');
		}
	  
		public function logout()
		{
			$this->session->unset_userdata('user');
			$this->session->sess_destroy();
			redirect(base_url());
		}   
			
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */