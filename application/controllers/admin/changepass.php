<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class changepass extends CI_Controller 
{

  public function __construct()
{
	parent::__construct();

	$this->load->model('admin/changepassword_model');
	$this->load->helper('form');  
}

	public function index()
	{     
		$data1['loginuser'] = $this->session->userdata('user');
		$data = [];
		$this->load->view('admin/includes/header',$data1);
		$this->load->view('admin/includes/page_header_sidebar');
		$this->load->view('admin/changepass/index', $data);
		$this->load->view('admin/includes/footer');
	}
		
		public function save()
		{
			$this->form_validation->set_rules('oldpassword', 'Old Password', 'required|trim|xss_clean');
			$this->form_validation->set_rules('newpassword', 'New Password', 'required|trim|xss_clean');
			if($this->form_validation->run() === FALSE)
			{
				$data = [];
				$data1['loginuser'] = $this->session->userdata('user');
				$this->load->view('admin/includes/header',$data1);
				$this->load->view('admin/includes/page_header_sidebar');
				$this->load->view('admin/changepass/index', $data);
				$this->load->view('admin/includes/footer');
			}
			else
			{
				$data = array();
				$where = array('email' => $this->session->userdata('user'));
				$data = array('password' => password_hash($this->input->post('newpassword'), PASSWORD_DEFAULT));
				$user_oldpass = $this->input->post('oldpassword'); 
				$confirm_pass = $this->input->post('confirmpassword');
				$new_password = $this->input->post('newpassword');
				$resoldpass = $this->changepassword_model->get_single_user_detail($this->session->userdata('user'));
				
				$oldpass = $resoldpass[0]['password'];


				if (password_verify($user_oldpass, $resoldpass[0]['password'])) {
					if($new_password == $confirm_pass){
						$res = $this->changepassword_model->update($data, $where);
						if($res)
						{
							$this->session->set_flashdata('success', '<div class="alert alert-success">Password Updated Successfully.</div>');
							
							redirect(site_url().'admin/changepass/index');
						}
						else
						{
							$this->session->set_flashdata('success', '<div class="alert alert-danger">Password Not Updated Successfully.</div>');
						}
						redirect(base_url().'admin/changepass/index');
					}else{
						$this->session->set_flashdata('success', '<div class="alert alert-danger">Confirm Password Not Match.</div>');
						redirect(base_url().'admin/changepass/index');
					}
				}else{
					$this->session->set_flashdata('success', '<div class="alert alert-danger">Please,  Enter Correct Old Password.</div>');
					redirect(base_url().'admin/changepass/index');
				}
					
			}
			
		} 
}	 