<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	    
	 public function logout()
	   {
		$this->session->unset_userdata('user');
		redirect(base_url().'admin/login');
	   
	   }
}