<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Index extends CI_Controller 
{
	public function __construct()
	{
			parent::__construct();
			if(empty($this->session->userdata('user'))){
				redirect(base_url().'admin/login');
			}
			$this->load->model('admin/category_model');
			$this->load->model('admin/users_model');
			$this->load->model('admin/complain_model');
			$this->load->helper('form');
	}
	public function index()
	{
		if($this->session->userdata('user')!=null)
		{
			$data['totalCategory']   = $this->category_model->getTotalCategory();
			$data['totalUsers']   = $this->users_model->getTotalUsers();
			$data['totalComplain']   = $this->complain_model->getTotalComplain();
			$data['totalPending']   = $this->complain_model->getComplainByType(['status' => 'p']);
			$data['totalCancelled']   = $this->complain_model->getComplainByType(['status' => 'c']);
			$data['totalProgress']   = $this->complain_model->getComplainByType(['status' => 'pro']);
			$data['totalSolved']   = $this->complain_model->getComplainByType(['status' => 's']);
			$data['remainingSms']   = $this->getRemainingSms();

			$data['loginuser'] = $this->session->userdata('user');
			$this->load->view('admin/includes/header',$data);
			$this->load->view('admin/includes/page_header_sidebar');
			$this->load->view('admin/index',$data);
			$this->load->view('admin/includes/footer');
		}else{
			redirect(base_url().'admin/login');
		}
	}

	public function getRemainingSms() {
		$ch = curl_init();
		$smsUrl = 'http://login.bulksmsgateway.in/userbalance.php?user=rikenpethad&password=123456&type=3';

			
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $smsUrl,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"apikey: somerandomuniquekey",
					"cache-control: no-cache",
					"content-type: application/x-www-form-urlencoded"
				),
			));

			$response = curl_exec($curl);
			$response = json_decode($response);
			$err = curl_error($curl);

			curl_close($curl);

			if ($err) {
				return 0;
			}
			return $response->remainingcredits;

	}
}
