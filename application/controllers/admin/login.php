<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
  public function __construct()
  {
			parent::__construct();
			if(!empty($this->session->userdata('user'))){
				redirect(base_url().'admin/index');
			}

			$this->load->model('admin/Login_model');
			$this->load->helper('form');
			
  }
		
	public function index(){

		$this->form_validation->set_rules('emailid', 'Username', 'required|trim|xss_clean');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean');
		
		if($this->form_validation->run() === FALSE){

			$this->load->view('admin/login/index');
		}
		else{
			/*echo password_hash("123456", PASSWORD_DEFAULT);
			exit;*/
			
			$res = $this->Login_model->check_login($where);
			$where=array("email"=>$this->input->post("emailid"));
			$passwordRes = $this->Login_model->check_login($where);
			if (password_verify($this->input->post("password"), $passwordRes[0]['password'])) {
				$res[0]['id']; 
				$this->session->set_userdata('user', $res[0]['email']);
				redirect(base_url().'admin/index');
			} else {
				$this->session->set_flashdata('error', '<div class="err">Please check your username and password.</div>');
				redirect(base_url().'admin/login/index');
			}

		}
	}
}

