<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ComplainList extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
	}

	public function index()
	{
		$userId = $this->input->post("user_id");

		$this->db->select('tbl_complain.*,tbl_category.category_name_eng,tbl_category.category_name_guj');
		$this->db->from('tbl_complain');
		$this->db->join('tbl_category','tbl_complain.category_id = tbl_category.id AND tbl_complain.user_id ='.$userId.'');
		$return = $this->db->get()->result_array();
		
		$complainArr = [];
		$responseData = ["code" =>200, "status" => "success", "data" =>$complainArr];
		if (!empty($return)) {
			foreach ($return as $key => $value) {
				if (!empty($value['image'])) {
					$imageFile = APP_WEBSERVICE_IMAGE.$value['image'];
				} else {
					$imageFile = APP_WEBSERVICE_IMAGE.'noimg.png';
				}
				$value['image'] = $imageFile;
				$value['complain_id']= 'CMP'.$value['id'];
				
				$complainArr[] = $value;
			}
			$responseData = ["code" =>200, "status" => "success", "data" =>["complain_list"=>$complainArr]];
		}


		echo json_encode($responseData);
	}

	public function category()
	{
		$this->db->select('*');
		$this->db->from('tbl_category');
		$categoryData = $this->db->get()->result_array();
		$defaultCat = ["0" => ["id" => 0, 'category_name_eng' => 'Select Category', 'category_name_guj' => 'સિલેક્ટ કેટેગરી']];
		$categoryData = array_merge($defaultCat, $categoryData);
		if (!empty($categoryData)) {
			$responseData = ["code" =>200, "status" => "success", "data" =>$categoryData];
		} else {
			$responseData = ["code" =>200, "status" => "success", "data" =>[]];
		}
		echo json_encode($responseData);
	}
}
