<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Create extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->helper('form');
	}

	public function index()
	{
		$user_id = $this->input->post("user_id");
		$category_id = $this->input->post("category_id");
		$message = $this->input->post('message') ?: '';
		$image = $this->input->post("image");

		if (!empty($user_id) && !empty($category_id)) {
			$fileName = "";
			if (!empty($image)) {
				$image = base64_decode($image);
				$fileName = str_shuffle(time()).'.png';
				file_put_contents(APP_WEBSERVICE_IMAGE_DIR.$fileName, $image);
			}
			$updateData["image"] = $fileName;

			$insertData = [
				'user_id' => $user_id,
				'category_id' => $category_id,
				'message' => $message,
				'image' => $fileName,
			];

			$this->db->insert('tbl_complain', $insertData);
			$responseData = ["code" =>200, "status" => "success", "data" =>"Complain registed successfully"];

		} else {
			$responseData = ["code" =>100, "status" => "fails", "data" =>"Please insert all details"];
		}

		echo json_encode($responseData);
	}

	public function update($id)
	{
		$user_id = $this->input->post("user_id");
		$category_id = $this->input->post("category_id");
		$message = $this->input->post('message') ?: '';
		$image = $this->input->post("image");
		
		if (!empty($user_id) && !empty($category_id)) {
			$updateData = [
				'user_id' => $user_id,
				'category_id' => $category_id,
				'message' => $message,
			];
			$fileName = "";

			if (!empty($image)) {
				$image = base64_decode($image);
				$fileName = str_shuffle(time()).'.png';
				file_put_contents(APP_WEBSERVICE_IMAGE_DIR.$fileName, $image);
				$updateData["image"] =$fileName;
			}
			$updateData["image"] =$fileName;

			$res = $this->db->update('tbl_complain', $updateData, ["id" => $id]);
			$responseData = ["code" =>200, "status" => "success", "data" =>"Complain updated successfully"];

		} else {
			$responseData = ["code" =>100, "status" => "fails", "data" =>"Please insert all details"];
		}

		echo json_encode($responseData);
	}

	public function edit()
	{
		$id = $this->input->get("id");
		
		if (!empty($id)) {
			$where=array("id"=>$id);

			$query=$this->db->get_where('tbl_complain',$where)->row();
			if (!empty($query->image)) {
				$imageFile = APP_WEBSERVICE_IMAGE.$query->image;
			} else {
				$imageFile = APP_WEBSERVICE_IMAGE.'noimg.png';
			}
				
			$query->image = $imageFile;
			$responseData = ["code" =>200, "status" => "success", "data" =>$query];
		} else {
			$responseData = ["code" =>100, "status" => "fails", "data" =>"Please insert complain ID"];
		}
		echo json_encode($responseData);
	}
	public function delete()
	{
		$id = $this->input->get("id");
		if (!empty($id)) {
			$res = $this->db->delete('tbl_complain', ["id"=>$id]);
			$responseData = ["code" =>200, "status" => "success", "data" =>"Complain deleted successfully"];
		} else {
			$responseData = ["code" =>100, "status" => "fails", "data" =>"Please insert complain ID"];
		}

		echo json_encode($responseData);
	}
}
