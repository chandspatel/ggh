<?php
class profile_model extends CI_Model {
	private $tablename;
	function __construct()
	{
		$this->tablenamealbums = 'albums';
		$this->tablenamealbumtype = 'albumtype';
		$this->tablenamefiles = 'files';
		$this->tablename = 'users';
		parent::__construct();
	}
	
	function get_albums($where = '')
	{  
		$this->load->database();
                if(empty($where))
                {
                    $query=$this->db->get($this->tablenamealbums);
                }
                else
                {
                    $query=$this->db->get_where($this->tablenamealbums,$where);
                }
		
                if($query->num_rows()){
			return $query->result_array();
		}else{
			return false;
		}
	} 

   function get_albumtype($where = '')
	{  
		$this->load->database();
                if(empty($where))
                {
                    $query=$this->db->get($this->tablenamealbumtype);
                }
                else
                {
                    $query=$this->db->get_where($this->tablenamealbumtype,$where);
                }
		
                if($query->num_rows()){
			return $query->result_array();
		}else{
			return false;
		}
	}
    function get_files($where = '')
	{  
		$this->load->database();
                if(empty($where))
                {
                    $query=$this->db->get($this->tablenamefiles);
                }
                else
                {
                    $query=$this->db->get_where($this->tablenamefiles,$where);
                }
		
                if($query->num_rows()){
			return $query->result_array();
		}else{
			return false;
		}
	}	
	
	
	function get_users($where = '')
	{  
		$this->load->database();
                if(empty($where))
                {
                    $query=$this->db->get($this->tablename);
                }
                else
                {
                    $query=$this->db->get_where($this->tablename,$where);
                }
		
                if($query->num_rows()){
			return $query->result_array();
		}else{
			return false;
		}
	}
     function get_user_detail($user_id = '')
        {
            $where = array('user_id' => $user_id);
            return $this->db->get_where("users", $where)->result_array();
        } 
     function get_single_users_detail($user_id = '')
        {
            $where = array('user_id' => $user_id);
            return $this->db->get_where($this->tablename, $where)->result_array();
        }   
	 public function update($data, $where)
        {
               $res = $this->db->update($this->tablename, $data, $where);
               if($res)
               {
                   return TRUE;
               }
               else
               {
                   return FALSE;
               }    
        }

		
	public function add($data)
        {   
               $res = $this->db->insert($this->tablename, $data);
               if($res)
               {
                   return TRUE;
               }
               else
               {
                   return FALSE;
               }    
        } 
		
		  public function delete($user_id)
        {
            /*$available_id = $this->db->get_where('stars', array('vStar_category' => $gallery_id))->num_rows();
            if($available_id > 0)
            {
                return FALSE;
            }
            */
            $where = array('user_id' => $user_id);
            $res = $this->db->delete($this->tablename, $where);
            if($res)
            {
                return TRUE;
            }
        }
       
}