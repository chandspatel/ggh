<?php
class complain_model extends CI_Model {
	private $tablename;
	function __construct()
	{
		$this->tablename = 'tbl_complain';
		parent::__construct();
	}
	function get_complains($where = '', $start = 0)
	{
		$this->load->database();
		$this->db->select('tbl_complain.*,tbl_category.category_name_eng,tbl_category.category_name_guj, tbl_users.mobile_no');
		$this->db->from('tbl_complain');
		//$this->db->limit('2', $start);
		$this->db->order_by('tbl_complain.id', 'desc');

		$this->db->join('tbl_users','tbl_users.id = tbl_complain.user_id');
		$this->db->join('tbl_category','tbl_complain.category_id = tbl_category.id ');
		$this->db->where($where);
		$complainSql = $this->db->get()->result_array();

		return $complainSql;
		//echo "<pre>";print_r($complainSql);exit;
	} 
	function getTotalComplain()
	{
		$this->load->database();
		$this->db->select('tbl_complain.*,tbl_category.category_name_eng,tbl_category.category_name_guj, tbl_users.mobile_no');
		$this->db->from('tbl_complain');

		$this->db->join('tbl_users','tbl_users.id = tbl_complain.user_id');
		$this->db->join('tbl_category','tbl_complain.category_id = tbl_category.id ');
		$totalCat = $this->db->get();
		$totalCat = $totalCat->num_rows();
		return $totalCat;
	}
	function getComplainByType($where)
	{
		$this->load->database();
		$this->db->select('tbl_complain.*,tbl_category.category_name_eng,tbl_category.category_name_guj, tbl_users.mobile_no');
		$this->db->from('tbl_complain');

		$this->db->join('tbl_users','tbl_users.id = tbl_complain.user_id');
		$this->db->join('tbl_category','tbl_complain.category_id = tbl_category.id ');
		$this->db->where($where);
		$totalCat = $this->db->get();
		$totalCat = $totalCat->num_rows();
		return $totalCat;
	}
	public function delete($id)
	{
		$where = array('id' => $id);
		$res = $this->db->delete($this->tablename, $where);
		if($res)
		{
			return TRUE;
		}
	}
}