<?php
class Login_model extends CI_Model {
	private $tablename;
	function __construct()
	{
		$this->tablename = 'tbl_admin';
		parent::__construct();
                $this->load->database();
	}
	
	function check_login($where)
	{  
		$query=$this->db->get_where($this->tablename,$where);
		if($query->num_rows()){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
}