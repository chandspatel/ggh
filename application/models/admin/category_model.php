<?php
class category_model extends CI_Model {
	private $tablename;
	function __construct()
	{
		$this->tablename = 'tbl_category';
		parent::__construct();
	}
	
	function getTotalCategory()
	{
		$this->load->database();
		if(empty($where))
		{
				$query=$this->db->get($this->tablename);
		}
		else
		{
				$query=$this->db->get_where($this->tablename,$where);
		}
		$totalCat = $query->num_rows();
		return $totalCat;
	}
	function get_category($where = '')
	{
		$this->load->database();
		if(empty($where))
		{
				$query=$this->db->get($this->tablename);
		}
		else
		{
				$query=$this->db->get_where($this->tablename,$where);
		}

		if($query->num_rows()){
			return $query->result_array();
		}else{
			return false;
		}
	} 

	function get_single_category_detail($id = '')
	{
		$where = array('id' => $id);
		return $this->db->get_where($this->tablename, $where)->row();
	}

	public function add($data)
	{
		 $res = $this->db->insert($this->tablename, $data);
		 if($res)
		 {
				 return TRUE;
		 }
		 else
		 {
				 return FALSE;
		 }    
	} 

	public function update($data, $where)
	{
				 $res = $this->db->update($this->tablename, $data, $where);
				 if($res)
				 {
						 return TRUE;
				 }
				 else
				 {
						 return FALSE;
				 }    
	}

	public function delete($surname_id)
	{
		$where = array('id' => $surname_id);
		$res = $this->db->delete($this->tablename, $where);
		if($res)
		{
			return TRUE;
		}
	}
}