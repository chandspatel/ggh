<?php
class changepassword_model extends CI_Model {
	private $tablename;
	function __construct()
	{
		$this->tablename = 'tbl_admin';
		parent::__construct();
	}
	   function get_single_user_detail($user_id = '')
        {
          $where = array('email' => $user_id); 
            return $this->db->get_where($this->tablename, $where)->result_array();
        }

      function get_admin($where = '')
	{  
		$this->load->database();
                if(empty($where))
                {
                    $query=$this->db->get($this->tablename);
                }
                else
                {
                    $query=$this->db->get_where($this->tablename,$where);
                }
		
                if($query->num_rows()){
			return $query->result_array();
		}else{
			return false;
		}
	} 		

		public function update($data, $where)
        {
               $res = $this->db->update($this->tablename, $data, $where);
               if($res)
               {
                   return TRUE;
               }
               else
               {
                   return FALSE;
               }    
        }
}