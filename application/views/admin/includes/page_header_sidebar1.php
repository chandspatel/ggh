		
			<!-- BEGIN SIDEBAR LEFT -->
			<div class="sidebar-left sidebar-nicescroller">
				<ul class="sidebar-menu">
					<li><a href="<?php echo site_url('admin/familydetailprofile/index');?>"><i class="fa fa-dashboard icon-sidebar"></i>Dashboard</a></li>
					<li>
						<a href="#fakelink">
							<i class="fa fa-flask icon-sidebar"></i>
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							Manage Family Profile
							<span class="badge badge-warning span-sidebar">2</span>
						</a>
						<ul class="submenu">
							<li><a href="<?php echo site_url('admin/familydetailprofile/index');?>">Family Profile</a></li>
							<li><a href="<?php echo site_url('admin/familydetailprofile/memberview');?>">Family Members</a></li>
						</ul>
					</li>
					<!--<li>
						<a href="#fakelink">
							<i class="fa fa-flask icon-sidebar"></i>
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							Reports
							<span class="badge badge-warning span-sidebar">1</span>
						</a>
						<ul class="submenu">
						    <li><a href="<?php echo site_url('admin/report/index');?>">Familys</a></li>
						</ul>
					</li>-->
					<li>
						<a href="#fakelink">
							<i class="fa fa-flask icon-sidebar"></i>
							<i class="fa fa-angle-right chevron-icon-sidebar"></i>
							Setting
							<span class="badge badge-warning span-sidebar">2</span>
						</a>
						<ul class="submenu">
							<li><a href="<?php echo site_url('admin/familychangepass/index');?>">Change Password</a></li>
							<li><a href="<?php echo site_url('admin/familylogout');?>">Logout</a></li>
						</ul>
					</li>
				
				</ul>
			</div><!-- /.sidebar-left -->
			<!-- END SIDEBAR LEFT -->
			
			