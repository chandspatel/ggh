		
				<!-- BEGIN FOOTER -->
				<!-- <footer>
					&copy; 2015 <a href="#fakelink">RJiTech Solution</a><br />
					Developed by : <a href="" target="_blank"></a> <a href="" target="_blank">RJiTech Solution</a>
				</footer> -->
			
				<!-- END FOOTER -->
				
						
			</div><!-- /.page-content -->
		</div><!-- /.wrapper -->
		
		<!--
		===========================================================
		Placed at the end of the document so the pages load faster
		===========================================================
		-->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<script src="<?php echo admin_public_path; ?>js/jquery.min.js"></script>
		<script src="<?php echo admin_public_path; ?>js/bootstrap.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/retina/retina.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/nicescroll/jquery.nicescroll.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/backstretch/jquery.backstretch.min.js"></script>
 
		<!-- PLUGINS -->
		<script src="<?php echo admin_public_path; ?>plugins/skycons/skycons.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/prettify/prettify.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/chosen/chosen.jquery.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/icheck/icheck.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/timepicker/bootstrap-timepicker.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/mask/jquery.mask.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/validator/bootstrapValidator.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/datatable/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/datatable/js/bootstrap.datatable.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/summernote/summernote.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/markdown/markdown.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/markdown/to-markdown.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/markdown/bootstrap-markdown.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/slider/bootstrap-slider.js"></script>
		
		<!-- EASY PIE CHART JS -->
		<script src="<?php echo admin_public_path; ?>plugins/easypie-chart/easypiechart.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/easypie-chart/jquery.easypiechart.min.js"></script>
		
		<!-- KNOB JS -->
		<!--[if IE]>
		<script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
		<![endif]-->
		<script src="<?php echo admin_public_path; ?>plugins/jquery-knob/jquery.knob.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/jquery-knob/knob.js"></script>

		<!-- FLOT CHART JS -->
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.tooltip.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.resize.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.selection.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.stack.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.time.js"></script>

		<!-- MORRIS JS -->
		<script src="<?php echo admin_public_path; ?>plugins/morris-chart/raphael.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/morris-chart/morris.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/morris-chart/example.js"></script>

		<!-- C3 JS -->
		<script src="<?php echo admin_public_path; ?>plugins/c3-chart/d3.v3.min.js" charset="utf-8"></script>
		<script src="<?php echo admin_public_path; ?>plugins/c3-chart/c3.min.js"></script>

		<!-- MAIN APPS JS -->
		<script src="<?php echo admin_public_path; ?>js/apps.js"></script>

 <!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
	
</div>
		<script type="text/javascript">
				$(document).ready(function(){

					$("#datatable-example").on("change", ".complain_status", function(){
						var id = $(this).data('id');
						var val = $(this).val();
						$.ajax({
							dataType: 'json',
							url: "<?php echo site_url.'admin/complain/updateStatus';?>",
							type: 'post',
							data:{'id':id, val:val},
							error: function (jqXHR, textStatus, errorThrown) {
							},
							success: function (data) {
								alert(data.msg);
							}
							});

					});

					$("#datatable-example").on("click", ".view_complain", function(){
						var id = $(this).data('id');

						$("#myModal").empty();

						$.ajax({
							dataType: 'html',
							url: "<?php echo site_url.'admin/complain/getDetails';?>",
							type: 'post',
							data:{'id':id},
							error: function (jqXHR, textStatus, errorThrown) {
								console.log(errorThrown);
							},
							success: function (dataHtml) {
								$("#myModal").append(dataHtml);
								$("#myModal").modal('show');
								console.log();
							}
							});

					});
					
				});
			</script>