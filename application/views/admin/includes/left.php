
 <div id="side-nav">
      <ul id="nav">
        <li class="current"> <a href="dashboard.php"> <i class="icon-dashboard"></i> Dashboard </a> </li>
              
        <li> <a href="#"> <i class="icon-edit"></i> Dynamic  <i class="arrow icon-angle-left"></i></a>
          <ul class="sub-menu opened">
            <li> <a href="dynamic_table.php"> <i class="icon-angle-right"></i>Dynamic table</a> </li>
            <li> <a href="form_validation.php"> <i class="icon-angle-right"></i>Form Validation</a> </li>
          </ul>
        </li>         
       
        <li> <a href="#"> <i class="icon-edit"></i> Gallery   <i class="arrow icon-angle-left"></i></a>
          <ul class="sub-menu">
            <li> <a href="gallery.php"> <i class="icon-angle-right"></i>Gallery</a> </li>
           
          </ul>
        </li>      
      </ul>
    </div>