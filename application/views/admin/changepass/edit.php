<!-- BEGIN PAGE CONTENT -->
			<div class="page-content">
	           <div class="container-fluid">
					<!-- Begin page heading -->
					<h1 class="page-heading">Change Password  <small></small></h1>
					<!-- End page heading -->
				
					<!-- Begin breadcrumb -->
					<ol class="breadcrumb default square rsaquo sm">
						<li><a href="index.html"><i class="fa fa-home"></i></a></li>
						<li><a href="<?php echo site_url('admin/changepass/index');?>">View Admin</a></li>
						<li><a href="<?php echo site_url('admin/changepass/edit');?>">Change Password</a></li>
						<li class="active"></li>
					</ol>
					<!-- End breadcrumb -->
					
					<?php echo $this->session->flashdata('success'); ?>
		        <?php echo validation_errors('<div class="alert alert-danger" style="color:#FF0000">','</div>'); ?>
					<div class="the-box">
						<?php echo $this->session->flashdata('success'); ?>
				        <?php echo validation_errors('<div class="alert alert-danger" style="color:#FF0000">','</div>'); ?>
                            <?php echo form_open_multipart('admin/changepass/edit/'.$user_id); ?>
							<fieldset>
								
					
								<div class="form-group">
									<label class="col-lg-3 control-label">Email Id</label>
									<div class="col-lg-5">
										<input type="text" class="form-control" name="emailid" id="emailid" placeholder="" value="<?php echo $detail[0]['emailid']?>">
									</div>
								</div>
							</fieldset>
							<br>
							<fieldset>
								

								<div class="form-group">
									<label class="col-lg-3 control-label">Old Password</label>
									<div class="col-lg-5">
										<input type="password" class="form-control" name="oldpassword" id="oldpassword" placeholder="Enter your Old Password" >
									</div>
								</div>
							</fieldset>
							<br>
							<fieldset>
								

								<div class="form-group">
									<label class="col-lg-3 control-label">New Password</label>
									<div class="col-lg-5">
										<input type="password" class="form-control" name="newpassword" id="newpassword" placeholder="Enter your New Password"/>
									</div>
								</div>
							</fieldset>
							<br>
							<fieldset>
								

								<div class="form-group">
									<label class="col-lg-3 control-label">Confirm Password</label>
									<div class="col-lg-5">
										<input type="password" class="form-control" name="confirmpassword" id="confirmpassword" placeholder="Enter your Confirm Password"/>
									</div>
								</div>
							</fieldset>
							   	<fieldset>
								<div class="col-lg-5"> <br></div>	
								</fieldset>
							<fieldset>
							<div class="form-group">
								<div class="col-lg-9 col-lg-offset-3">
									<button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
								</fieldset>
						</form>
					</div><!-- /.the-box -->
				</div><!-- /.container-fluid -->
	
			</div><!-- /.page-content -->