<!-- BEGIN PAGE CONTENT -->
			<div class="page-content">

				<div class="container-fluid">
					<!-- Begin page heading -->
					<h1 class="page-heading">Add Category  <small></small></h1>
					<!-- End page heading -->
				
					<!-- Begin breadcrumb -->
					<ol class="breadcrumb default square rsaquo sm">
						<li><a href="<?php echo site_url('admin/index');?>"><i class="fa fa-home"></i></a></li>
						<li><a href="<?php echo site_url('admin/surname/index');?>">Category</a></li>
						<li class="active">Add Category</li>
					</ol>
					<!-- End breadcrumb -->
					
					
					<div class="the-box">
						<?php echo $this->session->flashdata('success'); ?>
				        <?php echo validation_errors('<div class="alert alert-danger" style="color:#FF0000">','</div>'); ?>
                            <?php echo form_open_multipart('admin/category/add'); ?>
							<fieldset>

								<div class="form-group">
									<label class="col-lg-3 control-label">English Category Name</label>
									<div class="col-lg-5">
										<input type="text" class="form-control" name="category_name_eng" id="category_name_eng" placeholder="English Category name" title="Category is Required" required/>
									</div>
								</div>
							</fieldset>
							<fieldset>

								<div class="form-group">
									<br>
									<label class="col-lg-3 control-label">Gujarati Category Name</label>
									<div class="col-lg-5">
										<input type="text" class="form-control" name="category_name_guj" id="category_name_guj" placeholder="Gujarati Category name" title="Category is Required" required/>
									</div>
								</div>
							</fieldset>

							   	<fieldset>
								<div class="col-lg-5"> <br></div>	
								</fieldset>
							<fieldset>
							<div class="form-group">
								<div class="col-lg-9 col-lg-offset-3">
									<button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
								</fieldset>
						</form>
					</div><!-- /.the-box -->
				</div><!-- /.container-fluid -->
	
			</div><!-- /.page-content -->