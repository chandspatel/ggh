	<!-- BEGIN PAGE CONTENT -->
		<!-- BEGIN PAGE CONTENT -->
			<div class="page-content">
				<div class="container-fluid">
					<!-- Begin page heading -->
					<h1 class="page-heading">Categories <small></small></h1>
					<!-- End page heading -->
					<!-- Begin breadcrumb -->
					<ol class="breadcrumb default square rsaquo sm">
						<li><a href="<?php echo site_url('admin/index');?>"><i class="fa fa-home"></i></a></li>
						<li class="active">Categories</li>
						
					</ol>
					<div class="btn-group">
								<a href="<?php echo site_url('admin/category/add');?>"><button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add Category</button></a>
							</div>
							<?php /*<div class="btn-group">
								<button type="button" class="btn btn-danger"><i class="fa fa-ban"></i> Delete selected</button>
							</div> */ ?>
					<!-- End breadcrumb -->
					
					<!-- BEGIN DATA TABLE -->
					<div class="the-box">
					<?php echo $this->session->flashdata('success'); ?>
				        <?php echo validation_errors('<div class="alert alert-danger" style="color:#FF0000">','</div>'); ?>
						<div class="table-responsive">
						<table class="table table-striped table-hover" id="datatable-example">
							<thead class="the-box dark full">
								<tr>
									<th>English Category Name</th>
									<th>Gujarati Category Name</th>
									<th width="160px">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php 
						if($category){
						foreach($category as $catobj) {
							?>
                        <tr>
                            <td><?php echo $catobj['category_name_eng']; ?></td>
                            <td><?php echo $catobj['category_name_guj']; ?></td>
                            <td><a href="<?php echo base_url().'admin/category/edit/'. $catobj['id'] ?>" class="btn btn-success">Edit</a>
						    <a href="<?php echo base_url().'admin/category/delete/'. $catobj['id'] ?>" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger">Delete</a></td>
                        </tr>
						 <?php }
						 }else{ ?>
						   <p> No Data Found </p>
						<?php }?>
							</tbody>
						</table>
						</div><!-- /.table-responsive -->
					</div><!-- /.the-box .default -->
					<!-- END DATA TABLE -->
				</div><!-- /.container-fluid -->
			</div><!-- /.page-content -->