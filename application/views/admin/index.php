
			<!-- BEGIN PAGE CONTENT -->
			<div class="page-content">
				<div class="container-fluid">
				
				<!-- Begin page heading -->
				<h1 class="page-heading">DASHBOARD <small></small></h1>
				<!-- End page heading -->
				
					<!-- BEGIN EXAMPLE ALERT -->
					<div class="alert alert-warning alert-bold-border fade in alert-dismissable">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
					  <p><strong>Welcome!</strong></p>
					  
					</div>
					<!-- END EXAMPLE ALERT -->	
					<h4 class="text-center small-heading more-margin-bottom"></h4>

					
					<!-- BEGIN SiTE INFORMATIONS -->
					<div class="row">
						<div class="col-sm-3">
							<div class="the-box no-border bg-warning tiles-information">
								<a href="<?php echo site_url('admin/complain?type=p');?>">
								<i class="fa fa-shopping-cart icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>PENDING COMPLAINS</p>
									<h1 class="bolded">
									<?php
										echo $totalPending;
									?>
									</h1> 
									<div class="progress no-rounded progress-xs">
									  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									  </div>
									</div>
									<p><small></small></p>
								</div>
								</a>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="the-box no-border bg-primary tiles-information">
								<a href="<?php echo site_url('admin/complain?type=pro');?>">
								<i class="fa fa-shopping-cart icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>IN-PROGRESS COMPLAINS</p>
									<h1 class="bolded">
									<?php
										echo $totalProgress;
									?>
									</h1> 
									<div class="progress no-rounded progress-xs">
									  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									  </div>
									</div>
									<p><small></small></p>
								</div>
							</a>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="the-box no-border bg-success tiles-information">
								<a href="<?php echo site_url('admin/complain?type=s');?>">
									<i class="fa fa-shopping-cart icon-bg"></i>
									<div class="tiles-inner text-center">
										<p>SOLVED COMPLAINS</p>
										<h1 class="bolded">
										<?php
											echo $totalSolved;
										?>
										</h1> 
										<div class="progress no-rounded progress-xs">
										  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
										  </div>
										</div>
										<p><small></small></p>
									</div>
								</a>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="the-box no-border bg-danger tiles-information">
								<a href="<?php echo site_url('admin/complain?type=c');?>">
									<i class="fa fa-shopping-cart icon-bg"></i>
									<div class="tiles-inner text-center">
										<p>CANCELLED COMPLAINS</p>
										<h1 class="bolded">
										<?php
											echo $totalCancelled;
										?>
										</h1> 
										<div class="progress no-rounded progress-xs">
										  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
										  </div>
										</div>
										<p><small></small></p>
									</div>
								</a>
							</div>
						</div>

						<div class="col-sm-3">
							<div class="the-box no-border bg-primary tiles-information">
								<a href="<?php echo site_url('admin/complain');?>">
								
									<i class="fa fa-shopping-cart icon-bg"></i>
									<div class="tiles-inner text-center">
										<p>TOTAL COMPLAINS</p>
										<h1 class="bolded">
										<?php
											echo $totalComplain;
										?>
										</h1> 
										<div class="progress no-rounded progress-xs">
										  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
										  </div>
										</div>
										<p><small></small></p>
									</div>
								</a>
							</div>
						</div>

						<div class="col-sm-3">
							<div class="the-box no-border bg-success tiles-information">
								<a href="<?php echo site_url('admin/users');?>">
								
								<i class="fa fa-users icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>TOTAL USERS</p>
									<h1 class="bolded"><?php
										echo $totalUsers;
									?></h1> 
									<div class="progress no-rounded progress-xs">
									  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									  </div>
									</div>
									<p><small></small></p>
								</div>
							</a>
							</div>
						</div>
						<div class="col-sm-3">
							<div class="the-box no-border bg-danger tiles-information">
								<a href="<?php echo site_url('admin/category');?>">
								<i class="fa fa-comments icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>TOTAL CATEGORY</p>
									<h1 class="bolded"><?php
										echo $totalCategory;
									?></h1> 
									<div class="progress no-rounded progress-xs">
									  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									  </div>
									</div>
									<p><small><span class="text-danger"></span></small></p>
								</div>
							</a>
							</div>
						</div>

						<div class="col-sm-3">
							<div class="the-box no-border bg-warning tiles-information">
								<i class="fa fa-shopping-cart icon-bg"></i>
								<div class="tiles-inner text-center">
									<p>REMAINING SMS</p>
									<h1 class="bolded">
									<?php
										echo $remainingSms;
									?>
									</h1>
									<div class="progress no-rounded progress-xs">
									  <div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
									  </div>
									</div>
									<p><small></small></p>
								</div>
							</div>
						</div>

					</div>
				