<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<meta name="description" content="Sentir, Responsive admin and dashboard UI kits template">
		<meta name="keywords" content="admin,bootstrap,template,responsive admin,dashboard template,web apps template">
		<meta name="author" content="Ari Rusmanto, Isoh Design Studio, Warung Themes">
		<title>Login | Complain Management</title>
 
		<!-- BOOTSTRAP CSS (REQUIRED ALL PAGE)-->
		<link href="<?php echo admin_public_path; ?>css/bootstrap.min.css" rel="stylesheet">
		
		<!-- PLUGINS CSS -->
		<link href="<?php echo admin_public_path; ?>plugins/weather-icon/css/weather-icons.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/prettify/prettify.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/magnific-popup/magnific-popup.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/owl-carousel/owl.carousel.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/owl-carousel/owl.theme.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/owl-carousel/owl.transitions.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/chosen/chosen.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/icheck/skins/all.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/datepicker/datepicker.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/validator/bootstrapValidator.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/summernote/summernote.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/markdown/bootstrap-markdown.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/datatable/css/bootstrap.datatable.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/morris-chart/morris.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/c3-chart/c3.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>plugins/slider/slider.min.css" rel="stylesheet">
		
		<!-- MAIN CSS (REQUIRED ALL PAGE)-->
		<link href="<?php echo admin_public_path; ?>plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>css/style.css" rel="stylesheet">
		<link href="<?php echo admin_public_path; ?>css/style-responsive.css" rel="stylesheet">
		<style type="text/css">
			body.login {
				background: black !important;
				padding: 0;
				}

		</style>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
 
	<body class="login tooltips">
		<!--
		===========================================================
		BEGIN PAGE
		===========================================================
		-->
		<div class="login-header text-center">
			<img src="<?php echo admin_public_path; ?>img/logo-login.png" class="logo" alt="Logo">
		</div>
		<div class="login-wrapper">
		
		 <?php echo validation_errors('<div class="alert alert-danger" style="color:#FF0000">','</div>'); ?>
		  <?php 
		 if($this->session->flashdata('error')){?>
		 <div class="alert alert-warning alert-bold-border fade in alert-dismissable">
			  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			  <strong></strong><?php echo $this->session->flashdata('success'); ?>
			  <?php  echo '<div class="alert alert-danger" style="color:#FF0000">'.$this->session->flashdata('error').'</div>'; ?>
		
			</div>
		 
		<?php }
		 ?> 
			
			<?php echo form_open('admin/login/index'); ?>
				<div class="form-group has-feedback lg left-feedback no-label">
				  <input type="" class="form-control no-border input-lg rounded" id="emailid" name="emailid" placeholder="Enter Username" autofocus>
				  <span class="fa fa-user form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback lg left-feedback no-label">
				  <input type="password" class="form-control no-border input-lg rounded" id="password" name="password"  placeholder="Enter Password">
				  <span class="fa fa-unlock-alt form-control-feedback"></span>
				</div>
				<?php /*<div class="form-group">
				  <div class="checkbox">
					<label>
					  <input type="checkbox" class="i-yellow-flat"> Remember me
					</label>
				  </div>
				</div> */ ?>
				<div class="form-group">
					<button type="submit" class="btn btn-warning btn-lg btn-perspective btn-block">LOGIN</button>
				</div>
			</form>
			<?php /*<p class="text-center"><strong><a href="forgot-password.html">Forgot your password?</a></strong></p>
			<p class="text-center">or</p>
			<p class="text-center"><strong><a href="register.html">Create new account</a></strong></p> */ ?>
		</div><!-- /.login-wrapper -->
		<!--
		===========================================================
		END PAGE
		===========================================================
		-->
		
		<!--
		===========================================================
		Placed at the end of the document so the pages load faster
		===========================================================
		-->
		<!-- MAIN JAVASRCIPT (REQUIRED ALL PAGE)-->
		<script src="<?php echo admin_public_path; ?>js/jquery.min.js"></script>
		<script src="<?php echo admin_public_path; ?>js/bootstrap.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/retina/retina.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/nicescroll/jquery.nicescroll.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/slimscroll/jquery.slimscroll.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/backstretch/jquery.backstretch.min.js"></script>
 
		<!-- PLUGINS -->
		<script src="<?php echo admin_public_path; ?>plugins/skycons/skycons.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/prettify/prettify.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/owl-carousel/owl.carousel.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/chosen/chosen.jquery.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/icheck/icheck.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/datepicker/bootstrap-datepicker.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/timepicker/bootstrap-timepicker.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/mask/jquery.mask.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/validator/bootstrapValidator.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/datatable/js/jquery.dataTables.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/datatable/js/bootstrap.datatable.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/summernote/summernote.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/markdown/markdown.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/markdown/to-markdown.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/markdown/bootstrap-markdown.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/slider/bootstrap-slider.js"></script>
		
		<!-- EASY PIE CHART JS -->
		<script src="<?php echo admin_public_path; ?>plugins/easypie-chart/easypiechart.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/easypie-chart/jquery.easypiechart.min.js"></script>
		
		<!-- KNOB JS -->
		<!--[if IE]>
		<script type="text/javascript" src="assets/plugins/jquery-knob/excanvas.js"></script>
		<![endif]-->
		<script src="<?php echo admin_public_path; ?>plugins/jquery-knob/jquery.knob.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/jquery-knob/knob.js"></script>

		<!-- FLOT CHART JS -->
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.tooltip.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.resize.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.selection.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.stack.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/flot-chart/jquery.flot.time.js"></script>

		<!-- MORRIS JS -->
		<script src="<?php echo admin_public_path; ?>plugins/morris-chart/raphael.min.js"></script>
		<script src="<?php echo admin_public_path; ?>plugins/morris-chart/morris.min.js"></script>
		
		<!-- C3 JS -->
		<script src="<?php echo admin_public_path; ?>plugins/c3-chart/d3.v3.min.js" charset="utf-8"></script>
		<script src="<?php echo admin_public_path; ?>plugins/c3-chart/c3.min.js"></script>
		
		<!-- MAIN APPS JS -->
		<script src="<?php echo admin_public_path; ?>js/apps.js"></script>
		
	</body>
</html>