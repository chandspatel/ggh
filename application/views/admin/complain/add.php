<!-- BEGIN PAGE CONTENT -->
			<div class="page-content">

				<div class="container-fluid">
					<!-- Begin page heading -->
					<h1 class="page-heading">Add Surname  <small></small></h1>
					<!-- End page heading -->
				
					<!-- Begin breadcrumb -->
					<ol class="breadcrumb default square rsaquo sm">
						<li><a href="<?php echo site_url('admin/index');?>"><i class="fa fa-home"></i></a></li>
						<li><a href="<?php echo site_url('admin/surname/index');?>">Surname</a></li>
						<li class="active">Add Surname</li>
					</ol>
					<!-- End breadcrumb -->
					
					
					<div class="the-box">
						<?php echo $this->session->flashdata('success'); ?>
				        <?php echo validation_errors('<div class="alert alert-danger" style="color:#FF0000">','</div>'); ?>
                            <?php echo form_open_multipart('admin/surname/add'); ?>
							<fieldset>
								

								<div class="form-group">
									<label class="col-lg-3 control-label">Surname</label>
									<div class="col-lg-5">
										<input type="text" class="form-control" name="surname" id="surname" pattern="[A-Za-z ]+" title="Surname is Required" oninvalid="setCustomValidity('Surname is Required and Only alphabet characters use')" onchange="try{setCustomValidity('')}catch(e){}" required/>
									</div>
								</div>
							</fieldset>
							
							   	<fieldset>
								<div class="col-lg-5"> <br></div>	
								</fieldset>
							<fieldset>
							<div class="form-group">
								<div class="col-lg-9 col-lg-offset-3">
									<button type="submit" name="submit" id="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
								</fieldset>
						</form>
					</div><!-- /.the-box -->
				</div><!-- /.container-fluid -->
	
			</div><!-- /.page-content -->