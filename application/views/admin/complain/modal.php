<div class="modal-dialog">

		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Complain Details</h4>
			</div>
			<div class="modal-body">

				<p>Mobile No : <?php echo $mobile_no?> </p>
				<p>English Category : <?php echo $category_name_eng?> </p>
				<p>Gujarati Category : <?php echo $category_name_eng?> </p>
				<p>Image : <a target="_blank" href="<?php echo $image?>"> <img src="<?php echo $image?>" width=100 height=100 > </a> </p>
				<p>Complain Status : <?php echo $status?> </p>
				<p>Complain Date : <?php echo date('d-m-Y H:i:s', strtotime($created_date))?> </p>
				<p>Description : <?php echo $message;?> </p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>