	<!-- BEGIN PAGE CONTENT -->
		<!-- BEGIN PAGE CONTENT -->
			<div class="page-content">
				<div class="container-fluid">
					<!-- Begin page heading -->
					<h1 class="page-heading">Complain <small></small></h1>
					<!-- End page heading -->
					<!-- Begin breadcrumb -->
					<ol class="breadcrumb default square rsaquo sm">
						<li><a href="<?php echo site_url('admin/index');?>"><i class="fa fa-home"></i></a></li>
						<li class="active">Complain</li>
						
					</ol>
					<!-- <div class="btn-group">
								<a href="<?php //echo site_url('admin/surname/add');?>"><button type="button" class="btn btn-success"><i class="fa fa-plus"></i> Add Complain</button></a>
							</div> -->
							<?php /*<div class="btn-group">
								<button type="button" class="btn btn-danger"><i class="fa fa-ban"></i> Delete selected</button>
							</div> */ ?>
					<!-- End breadcrumb -->
					
					<!-- BEGIN DATA TABLE -->
					<div class="the-box">
					<?php echo $this->session->flashdata('success'); ?>
						<?php echo validation_errors('<div class="alert alert-danger" style="color:#FF0000">','</div>'); ?>
						<div class="table-responsive">
						<table class="table table-striped table-hover" id="datatable-example">
							<thead class="the-box dark full">
								<tr>
									<th>Complain No</th>
									<th>Mobile No</th>
									<th>Category</th>
									<th>Status</th>
									<th>Date</th>
									<th width="160px">Action</th>
								</tr>
							</thead>
							<tbody>
							<?php 
						if($complain){
						foreach($complain as $complainObj) {
							?>
						<tr>
							<td><?php echo $complainObj['complain_no']; ?></td>
							<td><?php echo $complainObj['mobile_no']; ?></td>
							<td><?php echo $complainObj['category_name_eng']; ?></td>
							<td>
								<select class="form-control complain_status" data-id="<?php echo $complainObj['id']; ?>">
									<?php echo $complainObj['status']; ?>
								</select>
								</td>
							<td><?php echo date("j M, Y",strtotime($complainObj['created_date'])); ?></td>
							<td><a href="javascript:void(0)" class="btn btn-success view_complain" data-id="<?php echo $complainObj['id'];?>">View</a>
							<a href="<?php echo base_url().'admin/complain/delete/'. $complainObj['id'] ?>" onclick="return confirm('Are you sure you want to delete this item?');" class="btn btn-danger">Delete</a></td>
							
						</tr>
						 <?php }
						 }else{ ?>
						   <p> No Data Found </p>
						<?php }?>
							</tbody>


						</table>
						<?php 

							//echo $this->pagination->create_links();
						?>
						</div><!-- /.table-responsive -->
					</div><!-- /.the-box .default -->
					<!-- END DATA TABLE -->
				</div><!-- /.container-fluid -->
			</div><!-- /.page-content -->
			